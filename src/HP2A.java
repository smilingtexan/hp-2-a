import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class HP2A extends JPanel  implements ActionListener {

	static Color Blue = new Color(0, 0, 200);
	static Color White = new Color(250, 250, 250);
	static JLabel HorsePowerValue = new JLabel();
	static JTextField HorsePower = new JTextField();
	static JLabel WattsValue = new JLabel();
	static JTextField Watts = new JTextField();
	static JLabel WattsUnits = new JLabel();
	static JLabel VoltsValue = new JLabel();
	static JTextField Volts = new JTextField();
	static JLabel VoltsUnits = new JLabel();
	static JLabel CurrentValue = new JLabel();
	static JTextField Current = new JTextField();
	static JLabel CurrentUnits = new JLabel();
	static JLabel StartCurrent = new JLabel();
	static JLabel StartCurrentValue = new JLabel();
	static JLabel StartCurrentUnits = new JLabel();

	public HP2A() {
		JPanel HorsePowerPanel = new JPanel();
		HorsePowerPanel.setLayout(new BoxLayout(HorsePowerPanel, BoxLayout.X_AXIS));
		HorsePowerValue.setText("HorsePower Rating: ");
		HorsePowerValue.setForeground(White);
		HorsePower.setColumns(15);
		HorsePower.setText("0");
		HorsePower.addActionListener(this);
		HorsePower.setActionCommand("HP_Change");
		HorsePowerPanel.setOpaque(false);
		HorsePowerPanel.add(HorsePowerValue);
		HorsePowerPanel.add(HorsePower);
		
		JPanel WattsPanel = new JPanel();
		WattsPanel.setLayout(new BoxLayout(WattsPanel, BoxLayout.X_AXIS));
		WattsValue.setText("Power Rating: ");
		WattsValue.setForeground(White);
		Watts.setColumns(15);
		Watts.setText("0");
		Watts.addActionListener(this);
		Watts.setActionCommand("Watts_Change");
		WattsUnits.setText(" Watts          ");
		WattsUnits.setForeground(White);
		WattsPanel.setOpaque(false);
		WattsPanel.add(WattsValue);
		WattsPanel.add(Watts);		
		WattsPanel.add(WattsUnits);
		
		JPanel VoltsPanel = new JPanel();
		VoltsPanel.setLayout(new BoxLayout(VoltsPanel, BoxLayout.X_AXIS));
		VoltsValue.setText("Voltage: ");
		VoltsValue.setForeground(White);
		Volts.setColumns(15);
		Volts.setText("125");
		Volts.addActionListener(this);
		Volts.setActionCommand("Volts_Change");
		VoltsUnits.setText(" Volts          ");
		VoltsUnits.setForeground(White);
		VoltsPanel.setOpaque(false);
		VoltsPanel.add(VoltsValue);
		VoltsPanel.add(Volts);
		VoltsPanel.add(VoltsUnits);
		
		JPanel CurrentPanel = new JPanel();
		CurrentPanel.setLayout(new BoxLayout(CurrentPanel, BoxLayout.X_AXIS));
		CurrentValue.setText("Current: ");
		CurrentValue.setForeground(White);
		Current.setColumns(15);
		Current.setText("0");
		Current.addActionListener(this);
		Current.setActionCommand("Current_Change");
		CurrentUnits.setText(" Amps          ");
		CurrentUnits.setForeground(White);
		CurrentPanel.setOpaque(false);
		CurrentPanel.add(CurrentValue);
		CurrentPanel.add(Current);
		CurrentPanel.add(CurrentUnits);
		
		JPanel StartupPanel = new JPanel();
		StartupPanel.setLayout(new BoxLayout(StartupPanel, BoxLayout.X_AXIS));
		StartCurrent.setText("Startup Current: ");
		StartCurrent.setForeground(White);
		StartCurrentValue.setText("0");
		StartCurrentValue.setForeground(White);
		StartCurrentUnits.setText(" Amps          ");
		StartCurrentUnits.setForeground(White);
		StartupPanel.setOpaque(false);
		StartupPanel.add(StartCurrent);
		StartupPanel.add(StartCurrentValue);
		StartupPanel.add(StartCurrentUnits);
		
		add(HorsePowerPanel, BorderLayout.PAGE_START);
		add(WattsPanel);
		add(VoltsPanel);
		add(CurrentPanel);
		add(StartupPanel);
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
}
	
	private static final long serialVersionUID = 1L;
	/**
	 * Setup the constraints so that the user supplied component and the background image
	 * label overlap and resize identically
	 * 
	 */

	public void actionPerformed(ActionEvent e) {
		if ("HP_Change".equals(e.getActionCommand())) {
			float temp = 0, temp1 = 0;
			temp = Float.parseFloat(HorsePower.getText());
			temp *= 746;		// 1HP = 746 Watts
			temp1 = Float.parseFloat(Volts.getText());
			temp1 = (temp / temp1);
			Watts.setText(String.valueOf(temp));
			Current.setText(String.valueOf(temp1));
			StartCurrentValue.setText(String.valueOf(temp1 * 3));
			Watts.grabFocus();
		}
		if ("Watts_Change".equals(e.getActionCommand())) {
			float temp = 0, temp1 = 0;
			temp = Float.parseFloat(Watts.getText());
			temp1 = Float.parseFloat(Volts.getText());
			temp1 = (temp / temp1);
			temp /= 746;		// 1HP = 746 Watts
			HorsePower.setText(String.valueOf(temp));
			Current.setText(String.valueOf(temp1));
			StartCurrentValue.setText(String.valueOf(temp1 * 3));
			Volts.grabFocus();
		}
		if ("Volts_Change".equals(e.getActionCommand())) {
			float temp = 0, temp1 = 0;
			temp = Float.parseFloat(Volts.getText());
			temp1 = Float.parseFloat(Watts.getText());
			temp1 /= temp;
			Current.setText(String.valueOf(temp1));
			StartCurrentValue.setText(String.valueOf(temp1 * 3));
			Current.grabFocus();
		}
		if ("Current_Change".equals(e.getActionCommand())) {
			float temp = 0, temp1 = 0;
			temp = Float.parseFloat(Current.getText());
			StartCurrentValue.setText(String.valueOf(temp * 3));
			temp1 = Float.parseFloat(Volts.getText());
			temp *= temp1;
			Watts.setText(String.valueOf(temp));
			HorsePower.setText(String.valueOf((temp / 746)));
		}
	}

	private static final GridBagConstraints gbc;

	static {
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
	}

	/**
	 * Wraps a Swing component in a background image
	 * Simply invokes the overloaded variant with Top/Leading alignment for background image
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon) {
		return wrapInBackgroundImage(component, backgroundIcon, JLabel.TOP, JLabel.LEADING);
	}
	
	/**
	 * Wraps a Swing component in a background image
	 * The vertical and horizontal alignment constrains the JLabel
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @param verticalAlignment - vertical Alignment
	 * @param horizontalAlignment - horizontal Alignment
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon, int verticalAlignment, int horizontalAlignment) {
		// make the passed in swing component transparent
		component.setOpaque(false);
		
		// create wrapper JPanel
		JPanel backgroundPanel = new JPanel(new GridBagLayout());
		
		// add the passed in swing component first to ensure it is in front
		backgroundPanel.add(component, gbc);
		
		// create a label to paint the background image
		JLabel backgroundImage = new JLabel(backgroundIcon);
		
		// set minimum and preferred sized so that the size of the image does no affect the layout size
		backgroundImage.setPreferredSize(new Dimension(1,1));
		backgroundImage.setMinimumSize(new Dimension(1,1));
		
		// align the image as specified
		backgroundImage.setVerticalAlignment(verticalAlignment);
		backgroundImage.setHorizontalAlignment(horizontalAlignment);
		
		// add the background label
		backgroundPanel.add(backgroundImage, gbc);
		
		// return the wrapper
		return backgroundPanel;
	}

	/**
	 * Returns an ImageIcon, or null if the path was invalid
	 *  
	 */
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = HP2A.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	public static void createAndShowGUI() {
		// get screen size & middle of screen position
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int windowX = Math.max(0, (screenSize.width - 400) / 2);
		int windowY = Math.max(0, (screenSize.height - 175) / 2);
		
		// Create and setup the window
		JFrame.setDefaultLookAndFeelDecorated(false);
		JFrame frame = new JFrame("HorsePower to Current Converter");
		
		// Create and setup the content pane
		JComponent foregroundPanel = new HP2A();
		foregroundPanel.setOpaque(true);

		frame.setContentPane(wrapInBackgroundImage(foregroundPanel, (createImageIcon("images/bg.jpg"))));
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) { System.exit(0); }
		});
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Display the window
		java.net.URL imgURL = HP2A.class.getResource("images/Adv-Icon.gif");
		frame.pack();
		frame.setIconImage(new ImageIcon(imgURL).getImage());
		frame.setBackground(Blue);
		frame.setResizable(false);
		frame.setSize(400, 175);
		frame.setLocation(windowX, windowY);
		frame.setVisible(true);
		
	}

	public static void main (String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}