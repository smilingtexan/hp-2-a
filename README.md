# HP 2 A
## by: Timothy Carter (aka: SmilingTexan)

This is a project I worked on some time ago. I was always being asked to convert from Horsepower to Amps.

This program does a simple conversion. It does not take anything into account,
and does a simple *theoretical* calculation; no losses or anything else is factored in.

It uses the fact that 1 HP (Horsepower) is equal to 746 Watts. There is an input
field for the voltage, such as 120 or 240 (or any other value). Then does a simple
Ohm's law calculation to get from power to current, of the formula
```
I = P/V
(Current, in Amps = Power, in watts divided by Voltage, in volts)
```

Use at your own risk.
